import { Component, OnInit } from '@angular/core';
import { ApiBridgeService } from '../api-bridge.service';

import { Observable, throwError, pipe, interval } from 'rxjs';
import { map, tap, timeInterval, timeout } from 'rxjs/operators';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.scss']
})
export class PlaygroundComponent implements OnInit {

  public posts: Array<any> = [];
  public check = false;

  constructor(private _ApiBridgeService: ApiBridgeService) { }

  ngOnInit(): void {
    this._ApiBridgeService.getRedditPosts(40)
      .pipe(
        map((e: any) => e.data.children),
        tap(e => {
          e.forEach((value: any) => {
            console.log(value.data);
            this.posts.push(value.data);
          })
          this.check = true;
        })
      ).subscribe()
  }
}
