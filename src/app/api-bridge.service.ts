import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiBridgeService {

  constructor(private http: HttpClient) { }

  getRedditPosts(limit: number): Observable<Object> {
    return this.http.get(`https://www.reddit.com/r/EarthPorn/top.json?limit=${limit}&t=day`,{});
  }
}
